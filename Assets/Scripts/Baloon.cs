using UnityEngine;

public class Baloon : MonoBehaviour
{
    public Rigidbody2D rb2D;
    public float power;

    public void Fire(Vector3 direction)
    {
        rb2D.AddForce(direction * power);
    }
    
    
}