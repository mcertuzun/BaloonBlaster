using UnityEngine;
using variable;

public class RotateController : MonoBehaviour
{
    private Vector3 mousePosition;
    public BaseVariable<Vector3> directionToMouse;
    private float angle;
    private Camera cam;

    private void Start()
    {
        cam = Camera.main;
    }

    private void Update()
    {
        mousePosition = cam.ScreenToWorldPoint(Input.mousePosition);
        mousePosition.z = 0;
        directionToMouse.value = mousePosition - transform.position;
        angle = (Mathf.Atan2(directionToMouse.value.y, directionToMouse.value.x) * Mathf.Rad2Deg - 90);
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }
}