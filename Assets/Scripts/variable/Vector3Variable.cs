using UnityEngine;

namespace variable
{
    [CreateAssetMenu(fileName = "Vector3Var", menuName = "CreateVariable/Vector3", order = 0)]
    public class Vector3Variable : BaseVariable<Vector3>
    {
    }
}