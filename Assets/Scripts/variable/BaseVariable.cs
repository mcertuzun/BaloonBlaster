using UnityEngine;

namespace variable
{
    public abstract class BaseVariable<T> : ScriptableObject
    {
        public T value;
    }
}