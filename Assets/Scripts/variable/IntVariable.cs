using UnityEngine;

namespace variable
{
    [CreateAssetMenu(fileName = "IntVar", menuName = "CreateVariable/Int", order = 0)]
    public class IntVariable : BaseVariable<int>
    {
    }
}