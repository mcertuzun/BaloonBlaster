using Lean.Touch;
using UnityEngine;
using variable;

public class Throw : MonoBehaviour
{
    public BaseVariable<Vector3> direction;
    public Baloon BaloonPrefab;

    private void Start()
    {
        LeanTouch.OnFingerUp += LeanTouchOnOnFingerUp;
    }

    private void LeanTouchOnOnFingerUp(LeanFinger obj)
    {
        var newBaloon = Instantiate(BaloonPrefab);
        BaloonPrefab.Fire(direction.value);
    }
}