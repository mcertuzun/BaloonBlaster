using Lean.Touch;
using UnityEngine;
using variable;

public class TrajectoryController : MonoBehaviour
{
    public LineRenderer lineRenderer;
    public Transform startTransform;
    public BaseVariable<Vector3> direction;
    public int reflections = 2;
    public float maxDistance = 100f;
    private bool isPress;

    private RaycastHit2D hit;

    private void Start()
    {
        lineRenderer.positionCount = reflections;
        LeanTouch.OnFingerUp += LeanTouchOnOnFingerUp;
        LeanTouch.OnFingerDown += LeanTouchOnOnFingerDown;
    }

    private void LeanTouchOnOnFingerDown(LeanFinger obj)
    {
        isPress = true;
    }

    private void LeanTouchOnOnFingerUp(LeanFinger obj)
    {
        isPress = false;
    }


    private void FixedUpdate()
    {
        if (isPress)
        {
            Debug.Log("VAR");
            //lineRenderer.SetPosition(0, startTransform.position);
            RaycastHit2D hit = Physics2D.CircleCast(startTransform.position, 0.1f, direction.value, maxDistance);

            if (hit.collider != null)
            {
                Debug.Log($"hitted to {hit.collider.name} pos {hit.point}");
            }
            /*
            direction.value.z = 90;
            hit = Physics2D.CircleCast(startTransform.position, 0.1f, direction.value, maxDistance);

            if (hit.collider != null)
            {
                Debug.Log($"hit wall {hit.point}");

                //lineRenderer.SetPosition(1, hit.point);
                DrawTrajectory((Vector2)startTransform.position, (Vector2)direction.value, 2);
            }
        */
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        RaycastHit2D hit = Physics2D.Raycast(startTransform.position, direction.value, Mathf.Infinity);

        if (hit != false)
        {
            Vector3 newpos = hit.point;
            newpos.z = 90;


            Gizmos.DrawSphere(newpos, 0.5f);
            Gizmos.DrawLine(startTransform.position, hit.point);
        }
    }

    private void DrawTrajectory(Vector2 origin, Vector2 velocity, int reflections)
    {
        lineRenderer.positionCount = 1;
        lineRenderer.SetPosition(0, origin);

        Vector2 currentPosition = origin;
        Vector2 currentVelocity = velocity;

        for (int i = 0; i < reflections; i++)
        {
            Vector2 nextPosition = GetNextReflection(currentPosition, currentVelocity);
            lineRenderer.positionCount += 1;
            lineRenderer.SetPosition(i + 1, nextPosition);

            currentPosition = nextPosition;
            currentVelocity = Vector2.Reflect(currentVelocity, GetReflectionNormal(currentPosition));
        }
    }

    private Vector2 GetNextReflection(Vector2 position, Vector2 velocity)
    {
        RaycastHit2D hit = Physics2D.Raycast(position, velocity, Mathf.Infinity);

        if (hit.collider != null)
        {
            return hit.point;
        }

        return position + velocity * 100f; // A far point if no reflection detected
    }

    private Vector2 GetReflectionNormal(Vector2 position)
    {
        Vector2 normal = Vector2.zero;

        // Add logic to calculate the reflection normal based on your canvas's borders

        return normal;
    }
}